import DefaultTemplate from "components/DefaultTemplate";
import Link from "next/link";

import { getProducts } from "api/product";
import { Row, Col, Table, Button } from "react-bootstrap";

export default () => {
	const { data, error } = getProducts();

	let toRender;

	if (error) toRender = <div>failed to load</div>;
	else if (!data) toRender = <div>loading...</div>;
	else
		toRender = (
			<Table responsive hover bordered>
				<thead>
					<tr>
						<td>Name</td>
						<td>Stock</td>
					</tr>
				</thead>
				<tbody>
					{data.data.map(product => (
						<Link
							href={"/products/[productID]"}
							key={product.id}
							as={`/products/${product.id}`}
						>
							<tr>
								<td>{product.name}</td>
								<td>{product.amount}</td>
							</tr>
						</Link>
					))}
				</tbody>
			</Table>
		);

	return (
		<DefaultTemplate>
			<Row>
				<Col>
					<Link
						href={`/products/create`}
						as={`/products/create`}
						passHref
					>
						<Button>Add Stock Event</Button>
					</Link>
				</Col>
			</Row>
			<Row>
				<Col>{toRender}</Col>
			</Row>
		</DefaultTemplate>
	);
};
