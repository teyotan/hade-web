import DefaultTemplate from "components/DefaultTemplate";

import { getProductDetail } from "api/product";
import { useRouter } from "next/router";
import { Row, Col, Table, Button } from "react-bootstrap";

import Link from "next/link";

export default () => {
	const router = useRouter();
	const { productID } = router.query;
	const { data, error } = getProductDetail(productID);

	let toRender;

	if (error) toRender = <div>failed to load</div>;
	else if (!data) toRender = <div>loading...</div>;
	else
		toRender = (
			<Table responsive hover bordered>
				<thead>
					<tr>
						<td>Date Time</td>
						<td>Price</td>
						<td>Amount</td>
						<td>Description</td>
					</tr>
				</thead>
				<tbody>
					{data.data.map(detail => (
						<tr key={detail.id}>
							<td>{new Date(detail.timestamp).toString()}</td>
							<td>{detail.price}</td>
							<td>
								{detail.type == "dec" ? "-" : null}
								{detail.amount}
							</td>
							<td>{detail.description}</td>
						</tr>
					))}
				</tbody>
			</Table>
		);

	return (
		<DefaultTemplate>
			<Row>
				<Col>
					<Link
						href={`/products/[productID]/create`}
						as={`/products/${productID}/create`}
						passHref
					>
						<Button>Add Stock Event</Button>
					</Link>
				</Col>
			</Row>
			<Row>
				<Col>{toRender}</Col>
			</Row>
		</DefaultTemplate>
	);
};
