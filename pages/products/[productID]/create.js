import DefaultTemplate from "components/DefaultTemplate";
import { useRouter } from "next/router";

import { createProductStockEvent } from "api/product";
import { Button, Form, Row, Col } from "react-bootstrap";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Router from "next/router";
import { useState } from "react";

export default () => {
	const router = useRouter();
	const { productID } = router.query;

	const [date, setDate] = useState(new Date());
	const [price, setPrice] = useState(0);
	const [amount, setAmount] = useState(0);
	const [description, setDescription] = useState("");

	return (
		<DefaultTemplate>
			<Form>
				<Form.Group as={Row} controlId="formPrice">
					<Form.Label column sm={2}>
						Price
					</Form.Label>
					<Col sm={10}>
						<Form.Control
							type="number"
							placeholder="0"
							value={price}
							onChange={e =>
								!isNaN(parseInt(e.target.value))
									? setPrice(parseInt(e.target.value))
									: setPrice(e.target.value)
							}
						/>
					</Col>
				</Form.Group>
				<Form.Group as={Row} controlId="formAmount">
					<Form.Label column sm={2}>
						Amount
					</Form.Label>
					<Col sm={10}>
						<Form.Control
							type="number"
							placeholder="0"
							value={amount}
							onChange={e =>
								!isNaN(parseInt(e.target.value))
									? setAmount(parseInt(e.target.value))
									: setAmount(e.target.value)
							}
						/>
					</Col>
				</Form.Group>
				<Form.Group as={Row} controlId="formDescription">
					<Form.Label column sm={2}>
						Description
					</Form.Label>
					<Col sm={10}>
						<Form.Control
							as="textarea"
							rows="2"
							value={description}
							onChange={e => setDescription(e.target.value)}
						/>
					</Col>
				</Form.Group>
				<Form.Group as={Row}>
					<Col sm={{ span: 10, offset: 2 }}>
						<DatePicker
							selected={date}
							onChange={date => setDate(date)}
							customInput={<Form.Control />}
						/>
					</Col>
				</Form.Group>
				<Form.Group as={Row}>
					<Col sm={{ span: 10, offset: 2 }}>
						<Button
							onClick={() =>
								createProductStockEvent({
									productID,
									date,
									price,
									amount,
									description
								}).then(() => Router.back())
							}
						>
							Create
						</Button>
						<Button onClick={() => Router.back()} variant="light">
							Back
						</Button>
					</Col>
				</Form.Group>
			</Form>
		</DefaultTemplate>
	);
};
