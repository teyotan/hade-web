import DefaultTemplate from "components/DefaultTemplate";

import { createProduct } from "api/product";
import { Button, Form, Row, Col } from "react-bootstrap";

import "react-datepicker/dist/react-datepicker.css";
import Router from "next/router";
import { useState } from "react";

export default () => {
	const [name, setName] = useState("");

	return (
		<DefaultTemplate>
			<Form>
				<Form.Group as={Row} controlId="formName">
					<Form.Label column sm={2}>
						Product Name
					</Form.Label>
					<Col sm={10}>
						<Form.Control
							type="text"
							value={name}
							onChange={e => setName(e.target.value)}
						/>
					</Col>
				</Form.Group>
				<Form.Group as={Row}>
					<Col sm={{ span: 10, offset: 2 }}>
						<Button
							onClick={() =>
								createProduct({
									name
								}).then(() => Router.back())
							}
						>
							Create
						</Button>
						<Button onClick={() => Router.back()} variant="light">
							Back
						</Button>
					</Col>
				</Form.Group>
			</Form>
		</DefaultTemplate>
	);
};
