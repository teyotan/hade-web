const path = require("path");

module.exports = {
	module: {
		rules: [
			{
				test: /\.css$/i,
				use: ["style-loader", "css-loader"]
			}
		]
	},
	resolve: {
		alias: {
			components: path.resolve(__dirname, "../", "components")
		}
	}
};
