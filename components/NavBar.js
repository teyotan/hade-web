import { Navbar, Nav, Container } from "react-bootstrap";

const NavBar = () => (
	<Navbar bg="light" expand="lg" className="mb-3">
		<Container>
			<Navbar.Brand href="/">Hade</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="mr-auto">
					<Nav.Link href="/products">Products</Nav.Link>
				</Nav>
			</Navbar.Collapse>
		</Container>
	</Navbar>
);

export default NavBar;
