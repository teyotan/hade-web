import DefaultTemplate from "components/DefaultTemplate";

export default {
	title: "DefaultTemplate"
};

export const withoutBody = () => <DefaultTemplate></DefaultTemplate>;
export const withBody = () => <DefaultTemplate>WithBody</DefaultTemplate>;
