import "bootstrap/dist/css/bootstrap.min.css";
import NavBar from "components/NavBar";
import { Container, Row, Col } from "react-bootstrap";

const DefaultLayout = props => (
	<div>
		<NavBar />
		<Container>
			<Row>
				<Col>{props.children}</Col>
			</Row>
		</Container>
		<style global jsx>{`
			.row {
				margin-bottom: 1rem !important;
			}
		`}</style>
	</div>
);

export default DefaultLayout;
