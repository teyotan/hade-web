const withCSS = require("@zeit/next-css");
const DotenvFlow = require("dotenv-flow-webpack");
const path = require("path");

module.exports = withCSS({
	poweredByHeader: false,
	webpack: config => {
		config.plugins.push(new DotenvFlow());
		config.resolve.alias["components"] = path.join(__dirname, "components");
		config.resolve.alias["api"] = path.join(__dirname, "api");
		return config;
	}
});
