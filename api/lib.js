import useSWR from "swr";
import fetch from "unfetch";

async function fetcher(path) {
	const res = await fetch(path);
	const json = await res.json();
	return json;
}

export const get = url => useSWR(url && process.env.API_URL + url, fetcher);

export const post = (url, body) =>
	fetch(process.env.API_URL + url, {
		method: "POST",
		body: JSON.stringify(body)
	});
