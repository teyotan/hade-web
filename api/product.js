import { get, post } from "./lib";

export const getProducts = () => get("product");

export const createProduct = ({ name }) => post("product", { name: name });

export const getProductDetail = productID =>
	get(productID && "product/" + productID + "/stock");

export const createProductStockEvent = ({
	productID,
	date,
	price,
	amount,
	description
}) => {
	let type = amount < 0 ? "dec" : "inc";
	return post(`product/${productID}/stock`, {
		date,
		price,
		amount,
		description,
		type
	});
};
